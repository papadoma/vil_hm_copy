This project is intended to collate Bristol VIL work that involves extending or modifying the functionality of the HM test model. This allows us to see, reuse and combine each others' work efficiently.

We are using HM release 16.2 as our starting point.

Suggested approach:
 - Create a new branch before changing anything -- i.e., don't work directly on the master branch, create a branch before changing someone else's work.
 - Keep to one major feature per branch from the master (e.g., one branch for forcing skip mode, another branch for dumping mode decisions to a text file) -- this makes it easier to fix bugs, test, and decide how to combine them later etc.
 - When a feature is finished/ready, tag that commit so that others can find it and use it. E.g., "force_skip_v1.0".
 - When a feature is finished, we can discuss how we merge with the main branch. For example, we need to decide what features of the vanilla HM are allowed to be broken by the addition of our changes.
 - Write your commit messages to be understandable by others.
 - Push and pull to and from (i.e., synchronise) the bitbucket repo regularly -- at least daily while you're actively working on the code -- to keep everyone up to date. Don't be shy about letting people see your work in progress!
 
Tips:
 - Git seems to behave better working from a local drive than a network drive.
 - The official Windows git GUI is pretty intuitive and is able to do pretty much everything you need.
 - Exercise caution when using the git command line -- it will let you do drastic things!
 - Remember to push and pull tags -- in the GUI there's a check box
 - Edit .gitignore to avoid committing non-code files: program output, yuv files, binaries, etc.

Original HM readme below 
***********************************************************

This software package is the reference software for Rec. ITU-T H.265 | ISO/IEC 23008-2 High efficiency video coding (HEVC). The reference software includes both encoder and decoder functionality.

Reference software is useful in aiding users of a video coding standard to establish and test conformance and interoperability, and to educate users and demonstrate the capabilities of the standard. For these purposes, this software is provided as an aid for the study and implementation of Rec. ITU-T H.265 | ISO/IEC 23008-2 High efficiency video coding.

The software has been jointly developed by the ITU-T Video Coding Experts Group (VCEG, Question 6 of ITU-T Study Group 16) and the ISO/IEC Moving Picture Experts Group (MPEG, Working Group 11 of Subcommittee 29 of ISO/IEC Joint Technical Committee 1).

A software manual, which contains usage instructions, can be found in the "doc" subdirectory of this software package.